package pl.edu.pjwstk.s11281.service;



import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import pl.edu.pjwstk.s11281.domain.User;

public class UserManagerTest {
	
	
	UserManager userManager = new UserManager();
	
	
	private final static String LOGIN_1 = "JakisLogin";
	private final static String PASSWORD_1 = "BardzoTajneHaslo";
		
	@Test
	public void checkConnection(){
		assertNotNull(userManager.getConnection());
	}
	
	@Test
	public void checkAdding(){
		
		User user = new User(LOGIN_1, PASSWORD_1);
		
		userManager.clearUsers();
		assertEquals(1,userManager.addUser(user));
		
		List<User> users = userManager.getAllUsers();
		User userRetrieved = users.get(0);
		
		assertEquals(LOGIN_1, userRetrieved.getLogin());
		assertEquals(PASSWORD_1, userRetrieved.getPassword());
					
		
	}

}


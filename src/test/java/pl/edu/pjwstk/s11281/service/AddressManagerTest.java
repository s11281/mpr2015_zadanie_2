package pl.edu.pjwstk.s11281.service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import pl.edu.pjwstk.s11281.domain.Address;

public class AddressManagerTest {
	
	
	AddressManager addressManager = new AddressManager();
	
	
	private final static String CITY_1 = "Gdansk";
	private final static String COUNTRY_1 = "Polska";
	private final static String HOUSENUMBER_1 = "123";
	private final static long IDPERSON_1=(long)1;
	private final static String LOCALNUMBER_1 = "12";
	private final static String POSTALCODE_1 = "80-800";
	private final static String STREET_1 = "Grunwaldzka";
	
	@Test
	public void checkConnection(){
		assertNotNull(addressManager.getConnection());
	}
	
	@Test
	public void checkAdding(){
		
		Address address =new Address(CITY_1, COUNTRY_1, HOUSENUMBER_1, IDPERSON_1, LOCALNUMBER_1, POSTALCODE_1, STREET_1);
				
		addressManager.clearAddress();
		assertEquals(1,addressManager.addAddress(address));
		
		List<Address> addresses = addressManager.getAllAddress();
		Address addressRetrieved = addresses.get(0);
		
		assertEquals(CITY_1, addressRetrieved.getCity());
		assertEquals(COUNTRY_1, addressRetrieved.getCountry());
		assertEquals(HOUSENUMBER_1, addressRetrieved.getHouseNumber());
		assertEquals(IDPERSON_1, addressRetrieved.getIdPerson());
		assertEquals(LOCALNUMBER_1, addressRetrieved.getLocalNumber());
		assertEquals(POSTALCODE_1, addressRetrieved.getPostalCode());
		assertEquals(STREET_1, addressRetrieved.getStreet());
		
			
		
	}

}

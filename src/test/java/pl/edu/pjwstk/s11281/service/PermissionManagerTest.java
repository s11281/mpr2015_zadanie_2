package pl.edu.pjwstk.s11281.service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import pl.edu.pjwstk.s11281.domain.Permission;

public class PermissionManagerTest {
	
	
	PermissionManager permissionManager = new PermissionManager();
	
	
	private final static long IDROLE_1 = 123;
	private final static String NAME_1 = "read";
	
	
	@Test
	public void checkConnection(){
		assertNotNull(permissionManager.getConnection());
	}
	
	@Test
	public void checkAdding(){
		
		Permission permission = new Permission(IDROLE_1,NAME_1);
		
		permissionManager.clearPermissions();
		assertEquals(1,permissionManager.addPermission(permission));
		
		List<Permission> permissions = permissionManager.getAllPermissions();
		Permission permissionRetrieved = permissions.get(0);
		
		assertEquals(IDROLE_1, permissionRetrieved.getIdRole());
		assertEquals(NAME_1, permissionRetrieved.getName());
				
		
	}

}

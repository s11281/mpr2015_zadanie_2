package pl.edu.pjwstk.s11281.service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import pl.edu.pjwstk.s11281.domain.Role;

public class RoleManagerTest {
	
	
	RoleManager roleManager = new RoleManager();
	
	
	private final static long IDPERSON_1=333123;
	private final static String NAME_1 = "administrator";
	
	
	@Test
	public void checkConnection(){
		assertNotNull(roleManager.getConnection());
	}
	
	@Test
	public void checkAdding(){
		
		Role role = new Role(IDPERSON_1,NAME_1);
		
		roleManager.clearRoles();
		assertEquals(1,roleManager.addRole(role));
		
		List<Role> roles = roleManager.getAllRoles();
		Role roleRetrieved = roles.get(0);
		
		assertEquals(IDPERSON_1, roleRetrieved.getIdPerson());
		assertEquals(NAME_1, roleRetrieved.getName());
				
		
	}

}

package pl.edu.pjwstk.s11281.service;


import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import pl.edu.pjwstk.s11281.domain.Person;

public class PersonManagerTest {
	
	
	PersonManager personManager = new PersonManager();
	
	
	private final static String FIRSTNAME_1 = "Jan";
	private final static String LASTNAME_1 = "Kowalski";
	private final static long IDUSER_1=(long)1;
	private final static String PHONENUMBER_1 = "+48583004050";
	
	
	@Test
	public void checkConnection(){
		assertNotNull(personManager.getConnection());
	}
	
	@Test
	public void checkAdding(){
		
		Person person = new Person(FIRSTNAME_1, LASTNAME_1,IDUSER_1,PHONENUMBER_1);
		
		personManager.clearPersons();
		assertEquals(1,personManager.addPerson(person));
		
		List<Person> persons = personManager.getAllPersons();
		Person personRetrieved = persons.get(0);
		
		assertEquals(FIRSTNAME_1, personRetrieved.getFirstName());
		assertEquals(LASTNAME_1, personRetrieved.getLastName());
		assertEquals(IDUSER_1, personRetrieved.getIdUser());
		assertEquals(PHONENUMBER_1, personRetrieved.getPhoneNumber());
			
			
		
	}

}

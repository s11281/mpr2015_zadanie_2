package pl.edu.pjwstk.s11281.domain;


public class Person {
	
	private long idUser;
	private long id;
	private String firstName;
	private String lastName;
	private String phoneNumber;
	
	

	public Person(String firstName, String lastName, Long idUser, String phoneNumber) {
		super();
		this.idUser=idUser;
		this.firstName=firstName;
		this.lastName=lastName;
		this.phoneNumber=phoneNumber;
	}


	public Person() {
		// TODO Auto-generated constructor stub
	}


	public long getIdUser() {
		return idUser;
	}

	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumer) {
		this.phoneNumber = phoneNumer;
	}
	
	
}
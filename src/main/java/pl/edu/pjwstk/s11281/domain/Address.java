package pl.edu.pjwstk.s11281.domain;

public class Address {
	
	private String country;
	private String city;
	private String street;
	private String postalCode;
	private String houseNumber;
	private String localNumber;
	private long idPerson;

	public Address(String city, String country, String houseNumber, long idPerson, String localNumber,
			String postalCode1, String street) {
		super();
		this.city=city;
		this.country=country;
		this.houseNumber=houseNumber;
		this.idPerson=idPerson;
		this.localNumber=localNumber;
		this.postalCode=postalCode1;
		this.street=street;
	}
	
	public Address() {
		// TODO Auto-generated constructor stub
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	public String getLocalNumber() {
		return localNumber;
	}
	public void setLocalNumber(String localNumber) {
		this.localNumber = localNumber;
	}
	public long getIdPerson() {
		return idPerson;
	}
	public void setIdPerson(long idPerson) {
		this.idPerson = idPerson;
	}
	
	
	}
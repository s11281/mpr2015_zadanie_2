package pl.edu.pjwstk.s11281.domain;

public class User {
	
	private long id;
	private String login;
	private String password;
	
		
	public User(String login, String password) {
		super();
		this.login = login;
		this.password = password;
	}

	public User() {
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	

}
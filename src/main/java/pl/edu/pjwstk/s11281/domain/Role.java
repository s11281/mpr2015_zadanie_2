package pl.edu.pjwstk.s11281.domain;


public class Role {

	private long idPerson;
	private long id;
	private String name;
		
	public Role(long idPerson, String name) {
		super();
		this.idPerson=idPerson;
		this.name=name;
	}

	public Role() {
		// TODO Auto-generated constructor stub
	}

	public long getIdPerson() {
		return idPerson;
	}

	public void setIdPerson(long idPerson) {
		this.idPerson = idPerson;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
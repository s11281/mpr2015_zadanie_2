package pl.edu.pjwstk.s11281.domain;

public class Permission {
	
	private long idRole;
	private String name;
	
	
	public Permission(long idRole, String name) {
		super();
		this.idRole=idRole;
		this.name=name;
	}

	public Permission() {
		// TODO Auto-generated constructor stub
	}

	public long getIdRole() {
		return idRole;
	}

	public void setIdRole(long idRole) {
		this.idRole = idRole;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
}